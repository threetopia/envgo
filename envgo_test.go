package envgo

import (
	"testing"
)

func TestEnvGo_LoadDotEnv(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
}

func TestEnvGo_GetString(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	str := GetString("TEST_GET_STRING", "")
	if str != "Loaded Correctly" {
		t.Errorf("Not Match (%s is wrong value)", str)
	}
}

func TestEnvGo_GetStringSlice(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	slc := [3]string{"ValueOne", "ValueTwo", "ValueThree"}
	// test with default delimiter
	slice := GetStringSlice("TEST_GET_STRING_SLICE", "")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%s is wrong value for %s)", slice[k], v)
		}
	}
	// test with custom delimiter
	slice = GetStringSlice("TEST_GET_STRING_SLICE_DELIMITER", ",")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%s is wrong value for %s)", slice[k], v)
		}
	}
}

func TestEnvGo_GetInt(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	integer := GetInt("TEST_GET_NUMBER", 0)
	if integer != 1234567890 {
		t.Errorf("Not Match (%d is wrong value)", integer)
	}
}

func TestEnvGo_GetIntSlice(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	slc := [5]int{1, 2, 3, 4, 5}
	// test with default delimiter
	slice := GetIntSlice("TEST_GET_NUMBER_SLICE", "")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%d is wrong value for %d)", slice[k], v)
		}
	}
	// test with custom delimiter
	slice = GetIntSlice("TEST_GET_NUMBER_SLICE_DELIMITER", ",")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%d is wrong value for %d)", slice[k], v)
		}
	}
}

func TestEnvGo_GetInt64(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	integer := GetInt64("TEST_GET_NUMBER", 0)
	if integer != int64(1234567890) {
		t.Errorf("Not Match (%d is wrong value)", integer)
	}
}

func TestEnvGo_GetInt64Slice(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	slc := [5]int64{1, 2, 3, 4, 5}
	// test with default delimiter
	slice := GetInt64Slice("TEST_GET_NUMBER_SLICE", "")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%d is wrong value for %d)", slice[k], v)
		}
	}
	// test with custom delimiter
	slice = GetInt64Slice("TEST_GET_NUMBER_SLICE_DELIMITER", ",")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%d is wrong value for %d)", slice[k], v)
		}
	}
}

func TestEnvGo_GetFloat32(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	float := GetFloat32("TEST_GET_NUMBER", 0)
	if float != float32(1234567890) {
		t.Errorf("Not Match (%f is wrong value)", float)
	}
}

func TestEnvGo_GetFloat64(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	float := GetFloat64("TEST_GET_NUMBER", 0)
	if float != float64(1234567890) {
		t.Errorf("Not Match (%f is wrong value)", float)
	}
}

func TestEnvGo_GetBool(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	isTrue := GetBool("TEST_GET_BOOL", false)
	if isTrue == false {
		t.Errorf("Not Match (%v is wrong value)", isTrue)
	}
}

func TestEnvGo_GetPort(t *testing.T) {
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	port := GetPort("TEST_GET_PORT", 0)
	if port != ":8080" {
		t.Errorf("Not Match (%v is wrong value)", port)
	}
}

func TestEnvGo_GetJson(t *testing.T) {
	var v struct {
		KeyOne   string `json:"key_one"`
		KeyTwo   string `json:"key_two"`
		KeyThree string `json:"key_three"`
		NumFour  int    `json:"num_four"`
	}
	err := LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}

	err = GetJson("TEST_GET_JSON", &v)
	if err != nil {
		t.Errorf("failed when get json data")
	}

	if v.KeyOne != "value_one" || v.KeyTwo != "value_two" || v.KeyThree != "value_three" || v.NumFour != 4 {
		t.Errorf("wrong value given (%v)", v)
	}
}
